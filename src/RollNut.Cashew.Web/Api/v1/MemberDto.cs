
namespace RollNut.Cashew.Web.Api.v1
{
    public class MemberDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
using RollNut.Cashew.Contract;
using RollNut.Cashew.Contract.Meter;

namespace RollNut.Cashew.Persistence.DbSchema
{
    public class MeterUnit
    {
        public int Id { get; set; }
        /// <inheritdoc cref="RollNut.Cashew.Domain.DbSchema.MeterType" />
        public MeterUnitType MeterUnitType { get; set; }
        public string Name { get; set; }
        public long InitialValue { get; set; }

        public IEnumerable<MeterReadingReceipt> MeterReadingReceipts { get; set; }
    }
}
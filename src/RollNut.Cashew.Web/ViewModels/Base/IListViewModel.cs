using System.Collections;

namespace RollNut.Cashew.Web.ViewModels.Base
{
    /// <summary>
    /// ViewModel which contains many items.
    /// The ListViewModel provides addittional informations,
    /// e.g. which item has been selected or if the list is readonly.
    /// Actions for the list are also placed here.
    /// <para>
    /// Info: Bind a list of ViewModels directly to View is prohibited 
    /// (for maintenance reasons).
    /// So a wrapper ViewModel ist required which contains the ViewModels.
    /// </para>
    /// </summary>
    public interface IListViewModel : IViewModel
    {
        /// <summary>
        /// The items to be displayed on frontend.
        /// </summary>
        public IEnumerable<IViewModel> Items { get; }

        /// <summary>
        /// Actions which the list provides.
        /// <para>
        /// Tipp: Exclude action entries if the user has no permission.
        /// </para>        
        /// </summary>
        public List<RollNutLink> ListActions { get; set; }

        public bool IsReadOnly { get; set; }

        public IEnumerable<string> GetColumnHeaders();
        public IEnumerable<object> GetColumnValues(object item);
    }

    /// <inheritdoc cref="IListViewModel"/>
    public interface IListViewModel<T> : IListViewModel
        where T : IViewModel
    {
        /// <inheritdoc/>
        IEnumerable<IViewModel> IListViewModel.Items { get => (IEnumerable<IViewModel>)Items; }
        /// <inheritdoc cref="IListViewModel.Items"/>
        IEnumerable<T> Items { get; }

         IEnumerable<object> IListViewModel.GetColumnValues(object item) => GetColumnValues((T)item);
        IEnumerable<object> GetColumnValues(T item);
    }

    /// <inheritdoc cref="IListViewModel"/>
    public abstract class ListViewModelBase<T> : IListViewModel<T>
        where T : IViewModel
    {
        private readonly List<T> _items;

        public ListViewModelBase()
        {
            _items = new List<T>();
            ListActions = new List<RollNutLink>();
        }

        public IEnumerable<T> Items { get => _items; }

        public T SelectedItem { get; set; }

        public bool IsReadOnly { get; set; }
        public List<RollNutLink> ListActions { get; set; }

        public virtual IEnumerable<string> GetColumnHeaders()
        {
            return new[] { "Name", };
        }

        public virtual IEnumerable<object> GetColumnValues(T viewModel)
        {
            ArgumentNullException.ThrowIfNull(viewModel);

            return new[] { viewModel.GetDisplayName() };
        }

        protected void AddItems(IEnumerable<T> items)
        {
            ArgumentNullException.ThrowIfNull(items);

            _items.AddRange(items);
        }

        public string GetDisplayName()
        {
            return $"List for {typeof(T).Name}";
        }
    }
}
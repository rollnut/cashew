namespace RollNut.Cashew.Domain.Meter.MeterUnitAggregate
{
    /// <summary>
    /// A snapshot of meter value from a meter entity.
    /// </summary>
    public class MeterReadingReceiptEntity : IDomainEntity<int>
    {
        /// <summary>
        /// This constructor is only allowed to be called by repository.
        /// </summary>
        public MeterReadingReceiptEntity(
            int id,
            decimal value, 
            DateTime valueReadingDate, 
            string remarks
            )
        {
            Id = id;
            Value = value;
            ValueReadingDate = valueReadingDate;
            Remarks = remarks;
        }

        /// <summary>
        /// Create a new entity with this constructor.
        /// </summary>
        public MeterReadingReceiptEntity(decimal value, DateTime valueReadingDate)
        {
            Value = value;
            ValueReadingDate = valueReadingDate;
        }

        /// <summary>
        /// The Id on persistence medium for this meter reading receipt.
        /// </summary>
        public int Id { get; }

        /// <summary>
        /// The meter reading value which was snapshotted on the meter value.
        /// </summary>
        public decimal Value { get; }

        /// <summary>
        /// Date on which the value was read from the meter.
        /// </summary>
        public DateTime ValueReadingDate { get; }

        /// <summary>
        /// Sometimes it exists an interesting story about a meter reading.
        /// </summary>
        public string? Remarks { get; }
    }
}
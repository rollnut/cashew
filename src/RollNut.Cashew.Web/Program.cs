using System.Globalization;
using MediatR;
using Microsoft.OpenApi.Models;
using RollNut.Cashew.Web;
using RollNut.Cashew.Domain;
using RollNut.Cashew.Persistence.DbSchema;
using RollNut.Cashew.Domain.Meter.MeterUnitAggregate;
using RollNut.Cashew.Persistence.Meter;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Options;

CultureInfo.DefaultThreadCurrentUICulture = Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddScoped<CashewLocalizer>();
builder.Services.AddControllersWithViews()
    .AddViewLocalization() // -> Activates IHtmlLocalizer und IViewLocalizer.
    //.AddDataAnnotationsLocalization(options =>
    //{
    //    options.DataAnnotationLocalizerProvider = (type, factory) =>
    //        factory.Create(typeof(Localizer));
    //})
    ;
builder.Services.AddLocalization(opt =>
{
    opt.ResourcesPath = "Localization";
});
//builder.Services.Configure<RequestLocalizationOptions>(options => 
//{
//    var supportedCultures = new List<CultureInfo>
//    {
//        new CultureInfo("en"),
//        new CultureInfo("de"),
//    };
//    options.DefaultRequestCulture = new RequestCulture("en-GB");
//    options.SupportedCultures = supportedCultures;
//    options.SupportedUICultures = supportedCultures;
//});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Version = "v1",
        Title = "RollNut Cashew Api v1",
    });
    options.IgnoreObsoleteActions();
    options.SwaggerGeneratorOptions.DocInclusionPredicate = (docname, apiDesc) => 
    {
        return apiDesc.ActionDescriptor.DisplayName.Contains("RollNut.Cashew.Web.Api");
    };
});
builder.Services.AddControllers();

// Add services to the container.
builder.Services.AddAutoMapper(typeof(RollNut.Cashew.Web.MapperProfile), typeof(RollNut.Cashew.Persistence.MapperProfile));
builder.Services.AddMediatR(
    typeof(RollNut.Cashew.Domain.Dummy), // Domain
    typeof(RollNut.Cashew.Persistence.MapperProfile) // Persistence
);
builder.Services.AddRazorPages();

builder.Services.AddTransient(typeof(CashewEssentials<>));
builder.Services.AddScoped<CashewSqliteDbContext>();
builder.Services.AddScoped<IMemberRepository, MemberRepository>();
builder.Services.AddScoped<IMeterUnitAggregateRepository, MeterUnitAggregateRepository>();
builder.Services.AddScoped<LinkLibrary>();


var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    // app.UseHsts();
}

// app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

// app.UseAuthorization();

// Set UICulture by incoming browser informations.
//var options = app.Services.GetService<IOptions<RequestLocalizationOptions>>();
//app.UseRequestLocalization(options!.Value);

var defaultCulture = app.Configuration.GetValue<string>("DefaultCulture", "en-GB");
app.UseRequestLocalization(defaultCulture);

app.MapRazorPages();
app.UseEndpoints(endpoints =>
{
    endpoints.MapControllerRoute(
        name: "default",
        pattern: "{controller=Home}/{action=Index}/{id?}");
});

app.Run();
namespace RollNut.Cashew.Contract
{
    public class Min : RangeAttribute
    {
        public Min(int minValue)
            : base(minValue, int.MaxValue)
        {
        }

        public Min(long minValue)
            : base(minValue, long.MaxValue)
        {
        }
    }
}
using System.ComponentModel.DataAnnotations;

namespace RollNut.Cashew.Contract.Meter
{
    public record MeterUnitCreateCommand : IRequest<MeterUnitCreateCommandResponse>
    {
        public MeterUnitCreateCommand(MeterUnitType meterUnitType, string name, long initialValue)
        {
            MeterUnitType = meterUnitType;
            Name = name;
            InitialValue = initialValue;
        }

        [Min(1)]
        public MeterUnitType MeterUnitType { get; init; }
        [Required]
        public string Name { get; init; }
        [Min(0L)]
        public long InitialValue { get; init; }
    }

    public class MeterUnitCreateCommandResponse
    {
    }
}
using RollNut.Cashew.Web.ViewModels.Base;

namespace RollNut.Cashew.Web.ViewModels
{
    public class MemberListViewModel : ListViewModelBase<MemberViewModel>
    {
        public MemberListViewModel(IEnumerable<MemberViewModel> members)
        {
            ArgumentNullException.ThrowIfNull(members);
            
            base.AddItems(members);

            var links = new LinkLibrary();
            ListActions.Add(links.Member.Create());
        }

        public override IEnumerable<string> GetColumnHeaders()
        {
            return new[]
            {
                "r",
                "u",
                "d",
                "Name",
            };
        }

        public override IEnumerable<object> GetColumnValues(MemberViewModel viewModel)
        {
            var links = new LinkLibrary();
            var id = viewModel.Id;

            return new object[]
            {
                links.Member.Read(id).GetHtmlLink(),
                links.Member.Update(id).GetHtmlLink(),
                links.Member.Delete(id),
                viewModel.GetDisplayName(),
            };
        }
    }
}
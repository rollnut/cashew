using RollNut.Cashew.Contract;
using RollNut.Cashew.Contract.Meter;

namespace RollNut.Cashew.Persistence.DbSchema
{
    public static class DbFactory
    {
        public static void FillTestData(CashewSqliteDbContext dbContext)
        {
            // DbMeta
            dbContext.DbMeta.Add(new DbMeta()
            {
                Version = typeof(DbFactory).Assembly.GetName().Version.ToString(),
            });
            // Member
            dbContext.Member.AddRange(
                new Member()
                {
                    Id = 1,
                    FirstName = "Allison",
                    LastName = "Zaupe",
                },
                new Member()
                {
                    Id = 2,
                    FirstName = "Berta",
                    LastName = "Ylloy",
                },
                new Member()
                {
                    Id = 3,
                    FirstName = "Colina",
                    LastName = "Xung",
                }
            );
            // Meter + MeterReceipt
            dbContext.MeterUnit.AddRange(
                new MeterUnit()
                {
                    MeterUnitType = MeterUnitType.Water,
                    Name = "W-123-XYZ",
                    InitialValue = 0,
                    MeterReadingReceipts = new List<MeterReadingReceipt>()
                    {
                    new MeterReadingReceipt()
                    {
                        Value = 5,
                        ValueDate = new DateTime(2021, 1, 1),
                    },
                    new MeterReadingReceipt()
                    {
                        Value = 8,
                        ValueDate = new DateTime(2021, 2, 1),
                    },
                    new MeterReadingReceipt()
                    {
                        Value = 14,
                        ValueDate = new DateTime(2021, 3, 1),
                    },
                    },
                },
                new MeterUnit()
                {
                    MeterUnitType = MeterUnitType.Water,
                    Name = "W-987-ABC",
                    InitialValue = 123456,
                    MeterReadingReceipts = new List<MeterReadingReceipt>()
                    {
                    new MeterReadingReceipt()
                    {
                        Value = 123564,
                        ValueDate = new DateTime(2021, 1, 1),
                    },
                    new MeterReadingReceipt()
                    {
                        Value = 123595,
                        ValueDate = new DateTime(2021, 2, 1),
                    },
                    }
                },
                new MeterUnit()
                {
                    MeterUnitType = MeterUnitType.Odometer,
                    Name = "O-444-GSX",
                    InitialValue = 19224,
                    MeterReadingReceipts = new List<MeterReadingReceipt>()
                    {
                    new MeterReadingReceipt()
                    {
                        Value = 19578,
                        ValueDate = new DateTime(2021, 1, 1),
                    },
                    new MeterReadingReceipt()
                    {
                        Value = 19745,
                        ValueDate = new DateTime(2021, 2, 1),
                    },
                    new MeterReadingReceipt()
                    {
                        Value = 19964,
                        ValueDate = new DateTime(2021, 3, 1),
                    },
                    new MeterReadingReceipt()
                    {
                        Value = 20046,
                        ValueDate = new DateTime(2021, 4, 1),
                    },
                    new MeterReadingReceipt()
                    {
                        Value = 20432,
                        ValueDate = new DateTime(2021, 5, 1),
                    },
                    }
                }
            );

            dbContext.SaveChanges();
        }
    }
}
namespace RollNut.Cashew.Contract.Meter
{
    /// <summary>
    /// A meter unit is a counting unit.
    /// This type defines what type of value is counted by that unit.
    /// </summary>
    public enum MeterUnitType
    {
        /// <summary>
        /// The meter type was not defined.
        /// </summary>
        Undefined = 0,
        /// <summary>
        /// Measuring of water consumption.
        /// </summary>
        Water = 1,
        /// <summary>
        /// Measuring of electric power consumption.
        /// </summary>
        Energy = 2,
        //Gas = 3,
        /// <summary>
        /// A meter of a vehicle (car, motorcycle, bicycle)
        /// which counts the driven distance.
        /// </summary>
        Odometer = 4,
        // HobbsMeter = 5
    }
}
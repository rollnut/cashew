﻿namespace RollNut.Cashew.Web.Base
{
    public enum LocalizeType
    {
        String,
        HtmlString,
    }

    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
    public class LocalizeTypeAttribute : Attribute
    {
        public LocalizeTypeAttribute(LocalizeType localizeType)
        {
            LocalizeType = localizeType;
        }

        public LocalizeType LocalizeType { get; }
    }
}

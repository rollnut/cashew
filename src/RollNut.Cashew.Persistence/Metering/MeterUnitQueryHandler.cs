using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using RollNut.Cashew.Contract;
using RollNut.Cashew.Contract.Meter;
using RollNut.Cashew.Persistence.DbSchema;

namespace RollNut.Cashew.Domain.Queries
{
    public class MeterUnitQueryHandler : IRequestHandler<MeterUnitQuery, MeterUnitQueryResponse>
    {
        private readonly CashewSqliteDbContext _dbContext;
        private readonly IMapper _mapper;

        public MeterUnitQueryHandler(CashewSqliteDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<MeterUnitQueryResponse> Handle(MeterUnitQuery request, CancellationToken cancellationToken)
        {
            var dtos = await _dbContext.MeterUnit
                .ProjectTo<MeterUnitDto>(_mapper.ConfigurationProvider)
                .ToListAsync()
                ;
            return new MeterUnitQueryResponse(dtos);
        }
    }
}
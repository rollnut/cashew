using Microsoft.AspNetCore.Mvc;
using RollNut.Cashew.Contract;

namespace RollNut.Cashew.Web.Links
{
    public abstract class LinkLibraryBase
    {
        protected string Concatenate(params object[] urlSegemnts)
        {
            if (urlSegemnts.Any(o => o is null)) throw new ArgumentException("An item in given array is null. Null values are not allowed.", nameof(urlSegemnts));
            return string.Join("/", urlSegemnts.Select(o => o.ToString().ToLower()));
        }

        protected string GetDisplayText(string key)
        {
            if (string.IsNullOrWhiteSpace(key)) throw new ArgumentException($"'{nameof(key)}' cannot be null or whitespace.", nameof(key));

            // TODO: Get link name from translation file.
            return key.SplitCamelCase(" ");
        }
    }

    /// <summary>
    /// Specialized link library where the links target one specific controller.
    /// </summary>
    /// <typeparam name="TController"></typeparam>
    public abstract class LinkLibraryBase<TController> : LinkLibraryBase
        where TController : Controller
    {
        public const string CREATE = "create";
        public const string READ = ""; // <-- Reading does not require a url segment.
        public const string UPDATE = "edit";
        public const string DELETE = "delete";

        private static readonly string ControllerClassName = typeof(TController).Name;

        /// <summary>
        /// Returns the relative link to target the controller.
        /// </summary>
        protected virtual string GetBaseLink()
        {
            // TODO: Read an alias attribute instead use class name.
            return ControllerClassName.Replace("Controller", "");
        }

        /// <summary>
        /// Creates the link object which targets the controller.
        /// </summary>
        protected virtual RollNutLink CreateControllerLink(params object[] pathSegments)
        {
            var joined = Concatenate(pathSegments);
            joined = Concatenate(GetBaseLink(), joined);

            if (!joined.StartsWith("/"))
            {
                joined = "/" + joined;
            }

            var link = new RollNutLink(joined);
            return link;
        }
    }
}
using RollNut.Cashew.Web.ViewModels.Base;

namespace RollNut.Cashew.Web.ViewModels.Meter
{
    public class MeterReadingReceiptViewModel : IEntityViewModel<int>
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Value { get; set; }

        public DateTime ValueDate { get; set; }
        public DateTime InputDate { get; set; }

        public bool CanDelete { get; set; }

        public string GetDisplayName()
        {
            return Name;
        }
    }
}
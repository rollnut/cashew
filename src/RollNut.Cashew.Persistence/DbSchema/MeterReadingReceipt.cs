namespace RollNut.Cashew.Persistence.DbSchema
{
    public class MeterReadingReceipt
    {
        public int Id { get; set; }

        [Obsolete("Is already placed on referenced meter unit.")]
        public int MeterUnitId { get; set; }
        public MeterUnit MeterUnit { get; set; }

        /// <summary>
        /// Value for the receipt.
        /// Can be the new meter value or other things.
        /// Depends on <see cref="ReceiptType"/>.
        /// </summary>
        public decimal Value { get; set; }

        /// <summary>
        /// Logical date for <see cref="Value"/>.
        /// The value date is not a receipt date.
        /// </summary>
        public DateTime ValueDate { get; set; }
    }
}
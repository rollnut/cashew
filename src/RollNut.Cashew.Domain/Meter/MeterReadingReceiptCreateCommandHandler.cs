using RollNut.Cashew.Contract.Meter;
using RollNut.Cashew.Domain.Meter.MeterUnitAggregate;

namespace RollNut.Cashew.Domain.Meter
{
    public class MeterReadingReceiptCreateCommandHandler : IRequestHandler<MeterReadingReceiptCreateCommand, MeterReadingReceiptCreateCommandResponse>
    {
        private readonly IMeterUnitAggregateRepository _repository;

        public MeterReadingReceiptCreateCommandHandler(IMeterUnitAggregateRepository repository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }
    
        public async Task<MeterReadingReceiptCreateCommandResponse> Handle(MeterReadingReceiptCreateCommand request, CancellationToken cancellationToken)
        {
            ArgumentNullException.ThrowIfNull(request);

            var aggregate = await _repository.GetByKeyAsync(request.MeterUnitId).ConfigureAwait(false);

            if (aggregate == null)
            {
                throw new ApplicationException("Aggregate not exists.");
            }

            aggregate.AddMeterReadingReceipt(request.MeterReadingReceipt.Value, request.MeterReadingReceipt.ValueDate);
            await _repository.UpdateAsync(aggregate).ConfigureAwait(false);
            await _repository.UnitOfWork.CommitAsync(cancellationToken).ConfigureAwait(false);

            return new MeterReadingReceiptCreateCommandResponse();
        }
    }
}
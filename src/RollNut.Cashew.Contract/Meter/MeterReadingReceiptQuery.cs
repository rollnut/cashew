using System.ComponentModel.DataAnnotations;
using MediatR;

namespace RollNut.Cashew.Contract.Meter
{
    public record MeterReadingReceiptQuery : IRequest<MeterReadingReceiptQueryResponse>
    {
        /// <summary>
        /// Load all receipts for this meter.
        /// </summary>
        [Range(1, int.MaxValue)]
        public int MeterUnitId { get; init; }
    }
    
    public record MeterReadingReceiptQueryResponse
    {
        public MeterReadingReceiptQueryResponse(IEnumerable<MeterReadingReceiptDto> results)
        {
            Results = results ?? throw new ArgumentNullException(nameof(results));
        }

        public IEnumerable<MeterReadingReceiptDto> Results { get; init; } = default!;
    }        
}
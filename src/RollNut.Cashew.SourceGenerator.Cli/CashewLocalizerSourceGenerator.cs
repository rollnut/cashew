﻿using RollNut.Cashew.Web.Base;
using System.Reflection;

namespace RollNut.Cashew.SourceGenerator.Cli
{
    internal partial class CashewLocalizerSourceGenerator
    {
        private readonly Type _extendableClass;
        private readonly SourceWriter _writer;

        public CashewLocalizerSourceGenerator(Type extendableClass, StreamWriter outputStream)
        {
            _extendableClass = extendableClass ?? throw new ArgumentNullException(nameof(extendableClass));
            if (outputStream == null) throw new ArgumentNullException(nameof(outputStream));
            _writer = new SourceWriter(outputStream);
        }

        public void Run()
        {
            _writer.WriteLine(Program.GetStamp);
            _writer.WriteLine($"using System;");
            _writer.WriteLine($"using Microsoft.AspNetCore.Html;");
            _writer.WriteLine();
            using (_writer.OpenScope($"namespace {_extendableClass.Namespace}"))
            {
                using (_writer.OpenScope($"public partial class {_extendableClass.Name}"))
                {
                    var fields = _extendableClass.GetFields();

                    foreach (var field in fields)
                    {
                        var localizeTypeAttribute = field.GetCustomAttributes(false)
                            .OfType<LocalizeTypeAttribute>()
                            .SingleOrDefault()
                            ;
                        if (localizeTypeAttribute != null)
                        {
                            WriteMethod(field, localizeTypeAttribute);
                        }

                        _writer.WriteLine();
                    }
                }
            }
        }

        private void WriteMethod(FieldInfo field, LocalizeTypeAttribute attribute)
        {
            if (field is null) throw new ArgumentNullException(nameof(field));
            if (attribute is null) throw new ArgumentNullException(nameof(attribute));

            string constantName = field.Name;
            string ResourceKeyName = field.GetValue(null)?.ToString() ?? String.Empty;
            string getMethodName = GetMethodName(field.Name);
            string getMethodResultType = attribute.LocalizeType.ToString();

            // Write summary.
            {
                _writer.WriteLine($"/// <summary>Returns localized <see cref='{getMethodResultType}'/> by constant <see cref='{constantName}'/> with key \"{ResourceKeyName}\".</summary>");
            }

            // Write method header and body.
            using (_writer.OpenScope($"public {getMethodResultType} Get{getMethodName}()"))
            {
                if (attribute.LocalizeType == LocalizeType.String)
                {
                    _writer.WriteLine($"return StringLocalizer[{constantName}];");
                }
                else if (attribute.LocalizeType == LocalizeType.HtmlString)
                {
                    _writer.WriteLine($"return new HtmlString(HtmlLocalizer[{constantName}].Value);");
                }
                else
                {
                    throw new ApplicationException($"Enum value '{attribute.LocalizeType}' unhandled.");
                }
            }
        }

        private string GetMethodName(string constantName)
        {
            var splitted = constantName.Split("_", StringSplitOptions.RemoveEmptyEntries);
            var lowered = splitted.Select(o => o.Substring(0, 1).ToUpper() + o.Substring(1).ToLower());
            return String.Join("", lowered);
        }
    }
}

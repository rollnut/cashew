using AutoMapper;
using RollNut.Cashew.Contract;
using RollNut.Cashew.Contract.Meter;
using RollNut.Cashew.Persistence.DbSchema;

namespace RollNut.Cashew.Persistence
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<Member, MemberDto>().ReverseMap();
            CreateMap<MeterUnit, MeterUnitDto>().ReverseMap();
            CreateMap<MeterReadingReceipt, MeterReadingReceiptDto>();
        }
    }
}
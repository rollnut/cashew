﻿namespace RollNut.Cashew.SourceGenerator.Cli
{
    /// <summary>
    /// A quick helper class to write nice C# code.
    /// </summary>
    internal class SourceWriter
    {
        private int _numberOfScopes = 0;
        private string _scopeIntendString = "    ";
        private readonly StreamWriter _outputStream;

        public SourceWriter(StreamWriter outputStream)
        {
            _outputStream = outputStream ?? throw new ArgumentNullException(nameof(outputStream));
        }

        public SourceScope OpenScope(string text)
        {
            WriteLine(text);
            var scope = new SourceScope(this);
            return scope;
        }

        public void OpenScope()
        {
            WriteLine("{");
            _numberOfScopes++;
        }

        public void CloseScope()
        {
            _numberOfScopes--;
            WriteLine("}");
        }

        private void WriteIntend()
        {
            for (int i = 0; i < _numberOfScopes; i++)
            {
                _outputStream.Write(_scopeIntendString);
            }
        }

        public void WriteLine(string text = "")
        {
            if (text is null) throw new ArgumentNullException(nameof(text));

            WriteIntend();
            _outputStream.WriteLine(text);
        }
    }

    internal class SourceScope : IDisposable
    {
        private readonly SourceWriter _sourceWriter;

        public SourceScope(SourceWriter sourceWriter)
        {
            _sourceWriter = sourceWriter ?? throw new ArgumentNullException(nameof(sourceWriter));
            _sourceWriter.OpenScope();
        }

        public void Dispose()
        {
            _sourceWriter.CloseScope();
        }
    }
}

using RollNut.Cashew.Web.ViewModels.Base;

namespace RollNut.Cashew.Web.ViewModels
{
    public class MemberViewModel : IEntityViewModel
    {
        object IEntityViewModel.Id => Id;

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string GetDisplayName()
        {
            return $"{FirstName} {LastName}";
        }
    }
}
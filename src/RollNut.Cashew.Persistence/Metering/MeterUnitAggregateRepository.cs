using RollNut.Cashew.Contract;
using RollNut.Cashew.Domain;
using RollNut.Cashew.Domain.Meter.MeterUnitAggregate;
using RollNut.Cashew.Persistence.DbSchema;

namespace RollNut.Cashew.Persistence.Meter
{
    public class MeterUnitAggregateRepository : IMeterUnitAggregateRepository
    {
        private readonly CashewSqliteDbContext _dbContext;

        public MeterUnitAggregateRepository(CashewSqliteDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public IUnitOfWork UnitOfWork { get => _dbContext; }

        public async Task<MeterUnitAggregateRoot?> GetByKeyAsync(int id)
        {
            MeterUnitAggregateRoot? aggregate;

            var table = await _dbContext.MeterUnit
                .Include(o => o.MeterReadingReceipts)
                .FirstOrDefaultAsync(o => o.Id == id)
                .ConfigureAwait(false)
                ;

            if (table == null)
            {
                aggregate = null;
            }
            else
            {
                aggregate = new MeterUnitAggregateRoot(
                    id: table.Id, 
                    meterUnitType: table.MeterUnitType, 
                    name: table.Name,
                    measureUnit: MeasureUnit.Undefined,
                    initialCount: table.InitialValue,
                    remarks: "n/a",
                    meterReadingReceipts: table.MeterReadingReceipts
                        .Select(mr => new MeterReadingReceiptEntity(default, default, default, default))
                    
                );
            }

            return aggregate;
        }

        public async Task UpdateAsync(MeterUnitAggregateRoot aggregateRoot)
        {
            ArgumentNullException.ThrowIfNull(aggregateRoot);

            foreach (var addedItem in aggregateRoot.AddedItems)
            {
                var test = (MeterReadingReceiptEntity)addedItem;
                var receipt = new MeterReadingReceipt()
                {
                    MeterUnitId = aggregateRoot.Id,
                    Value = test.Value,
                };
                _dbContext.Add(receipt);
            }

            foreach (var removedItem in aggregateRoot.RemovedItems)
            {
                var table = _dbContext.MeterReadingReceipt.First(o => o.Id.Equals(removedItem.Id));
                _dbContext.MeterReadingReceipt.Remove(table);
            }

            await Task.CompletedTask;
        }

        public async Task DeleteAsync(MeterUnitAggregateRoot aggregateRoot)
        {
            ArgumentNullException.ThrowIfNull(aggregateRoot);

            var table = await _dbContext.MeterUnit.FirstOrDefaultAsync(o => o.Id == aggregateRoot.Id);
            
            if (table != null)
            {
                _dbContext.Remove(table);
            }
        }
    }
}
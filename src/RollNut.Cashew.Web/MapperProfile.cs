using AutoMapper;
using RollNut.Cashew.Contract;
using RollNut.Cashew.Contract.Meter;
using RollNut.Cashew.Web.ViewModels;
using RollNut.Cashew.Web.ViewModels.Meter;

namespace RollNut.Cashew.Web
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<MemberDto, MemberViewModel>().ReverseMap();
            CreateMap<MeterUnitDto, MeterUnitViewModel>().ReverseMap();
            CreateMap<MeterReadingReceiptDto, MeterReadingReceiptViewModel>().ReverseMap();
        }
    }
}
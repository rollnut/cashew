using RollNut.Cashew.Contract.Meter;
using RollNut.Cashew.Web.ViewModels.Base;

namespace RollNut.Cashew.Web.ViewModels.Meter
{
    public class MeterReadingReceiptListViewModel : ListViewModelBase<MeterReadingReceiptViewModel>
    {
        public MeterReadingReceiptListViewModel(int meterUnitId, MeterUnitType meterUnitType, string meterUnitName, IEnumerable<MeterReadingReceiptViewModel> receipts)
        {
            ArgumentNullException.ThrowIfNull(receipts);

            var links = new LinkLibrary();

            AddItems(receipts);            

            ListActions.Add(links.MeterReadingReceipt.Create(meterUnitId));
            ListActions.Add(links.MeterReadingReceipt.DeleteNewest(meterUnitId));
            MeterUnitId = meterUnitId;
            MeterUnitType = meterUnitType;
            MeterUnitName = meterUnitName;
        }

        public int MeterUnitId { get; set; }
        public MeterUnitType MeterUnitType { get; set; }
        public string MeterUnitName { get; set; }

        public override IEnumerable<string> GetColumnHeaders()
        {
            return new[]
            {
                "d",
                "Value",
                "ValueDate",
                "InputDate",
            };
        }

        public override IEnumerable<object> GetColumnValues(MeterReadingReceiptViewModel viewModel)
        {
            return new object[]
            {
                viewModel.Value,
                viewModel.ValueDate,
                viewModel.InputDate,
            };
        }
    }
}
namespace RollNut.Cashew.Persistence.DbSchema
{
    public class DbMeta
    {
        public int Id { get; set; }
        /// <summary>
        /// The version of the db schema.
        /// </summary>
        public string Version { get; set; }
    }
}
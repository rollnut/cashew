
using Microsoft.AspNetCore.Html;
using RollNut.Cashew.Web.Links;

namespace RollNut.Cashew.Web
{
    /// <summary>
    /// Represents a link which can be printed and called by html.
    /// <para>
    /// This class compete the wiht Asp-ActionLink.
    /// This class is more type safe at compile time.
    /// </para>
    /// </summary>
    /// <remarks>
    /// Why this class is a record? 
    /// Because to use the auto generated equals method.
    /// A link should be equal because of their values instead by reference.
    /// <para>
    /// TODO: Read display text by translation file.
    /// </para>
    /// </remarks>
    public record RollNutLink
    {
        //public List<string> _cssClasses = new List<string>();

        public RollNutLink(string link)
        {
            ArgumentNullException.ThrowIfNull(link);
            if (string.IsNullOrWhiteSpace(link)) throw new ArgumentException("Link can not be empty or white spaced.", nameof(link));

            Link = link;
            DisplayText = link;
        }

        /// <summary>
        /// The hyperlink navigate to.
        /// </summary>
        public string Link { get; }

        /// <summary>
        /// The text which is shown to the user instead the raw link.
        /// If null then <see cref="Link"/> is used for display.
        /// </summary>
        public string? DisplayText { get; init; }

        //public string? ShortDisplayText { get; }
        //public string? Description { get; }

        /// <summary>
        /// Categorize the type of this link.
        /// In common a css class would be added to the html element 
        /// which represents the link type.
        /// <para>The default value is <see cref="LinkType.Neutral"/>.</para>
        /// </summary>
        public LinkType LinkType { get; init; }

        //public IReadOnlyCollection<string> CssClasses { get => _cssClasses.AsReadOnly(); }        

        public HtmlString GetHtmlLink()
        {
            string style = "";
             
            // if (CssClasses.Any())
            // {
            //     var classes = string.Join(" ", CssClasses.Select(o => o.ToLower()));
            //     style = $"style=\"{classes}\"";
            // }

            return new HtmlString(string.Format($"<a {style} href=\"{Link}\">{DisplayText}</a>"));
        }

        // Methods - Setters

        /// <inheritdoc cref="DisplayText"/>
        public RollNutLink SetDisplayText(string displayText)
        {
            return this with { DisplayText = displayText };
        }

        /// <summary>
        /// Change the value of <see cref="LinkType"/>
        /// to other than <see cref="LinkType.Neutral"/>.
        /// </summary>
        public RollNutLink SetLinkType(LinkType linkType)
        {
            return this with { LinkType = linkType };
        }

        // public RollNutLink AddCssClass(string className)
        // {
        //     if (string.IsNullOrWhiteSpace(className)) throw new ArgumentException($"'{nameof(className)}' cannot be null or whitespace.", nameof(className));

        //     _cssClasses.Add(className);
        //     return this;
        // }

        public override string ToString()
        {
            return Link;
        }
    }
}
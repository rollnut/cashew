using RollNut.Cashew.Contract;
using RollNut.Cashew.Contract.Meter;
using RollNut.Cashew.Web.ViewModels.Base;

namespace RollNut.Cashew.Web.ViewModels.Meter
{
    public class MeterUnitViewModel : IEntityViewModel<int>
    {
        public int Id { get; set; }

        [Min(1)]
        public MeterUnitType MeterUnitType { get; set; }

        public string Name { get; set; }

        public long InitialValue { get; set; }

        public string GetDisplayName()
        {
            return Name;
        }
    }
}
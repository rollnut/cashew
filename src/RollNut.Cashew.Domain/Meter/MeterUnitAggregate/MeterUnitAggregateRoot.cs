using RollNut.Cashew.Contract;
using RollNut.Cashew.Contract.Meter;

namespace RollNut.Cashew.Domain.Meter.MeterUnitAggregate
{
    /// <summary>
    /// A meter unit with receipts of meter readings.
    /// <para>
    /// The meter unit is a phyisical unit which counts a value like energy or water consumption.
    /// Periodically a receipt with the meter reading can be taken from the meter unit which 
    /// is a snapshot for that moment. That reading can be added to this meter.
    /// </para>
    /// </summary>
    public class MeterUnitAggregateRoot : IAggregateRoot<int>
    {
        // Fields

        private readonly List<MeterReadingReceiptEntity> _meterReceipts = new ();

        // Constructor

        /// <summary>
        /// This constructor is only allowed to be called by repository.
        /// </summary>
        public MeterUnitAggregateRoot(
            int id, 
            MeterUnitType meterUnitType, 
            string name, 
            MeasureUnit measureUnit, 
            decimal initialCount, 
            string remarks, 
            IEnumerable<MeterReadingReceiptEntity> meterReadingReceipts
        )
        {
            Id = id;
            MeterUnitType = meterUnitType;
            Name = name;
            ValueUnit = measureUnit;
            InitialMeterValue = initialCount;
            Remarks = remarks;

            foreach (var item in meterReadingReceipts)
            {
                _meterReceipts.Add(item);
            }
        }

        // Properties

        /// <summary>
        /// The Id on persistence medium for this meter.
        /// </summary>
        public int Id { get; }

        /// <summary>
        /// The type of this meter unit.
        /// </summary>
        public MeterUnitType MeterUnitType { get; }

        /// <summary>
        /// A human readable name or number for this meter unit.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Some details about this meter unit.
        /// </summary>
        public string Remarks { get; }

        /// <summary>
        /// The measure unit for the meter value.
        /// This unit is used for the meter readings too.
        /// </summary>
        public MeasureUnit ValueUnit { get; }

        /// <summary>
        /// The start value for this meter unit.
        /// </summary>
        public decimal InitialMeterValue { get; set; }

        /// <summary>
        /// The current value for this meter unit.
        /// </summary>
        public decimal CurrentMeterValue { get => InitialMeterValue + MeterReadingReceipts.Sum(o => o.Value); }

        /// <summary>
        /// All meter reading receipts that belong to this meter.
        /// </summary>
        public IReadOnlyCollection<MeterReadingReceiptEntity> MeterReadingReceipts { get => _meterReceipts.AsReadOnly(); }

        // Methods

        /// <summary>
        /// Returns true if this meter unit can be deleted.
        /// </summary>
        public bool CanDelete()
        {
            return !MeterReadingReceipts.Any();
        }

        /// <summary>
        /// <summary>
        /// Append a receipt at the end of the receipt list.
        /// </summary> 
        /// </summary>
        /// <param name="value">The meter reading value.
        /// If there is already a meter reading with higher value
        /// then an error happens.
        /// </param>
        /// <param name="valueReadingDate">
        /// Date on which the value was read from the meter.
        /// If there is already a meter reading with higher date
        /// then an error happens.
        /// </param>
        public void AddMeterReadingReceipt(decimal value, DateTime valueReadingDate)
        {
            if (value < 0) throw new ArgumentException("Not allowed to be zero or lower.", nameof(value));

            var maxValue = MeterReadingReceipts.MaxOrDefault(o => o.Value);
            var maxValueReadingDate = MeterReadingReceipts.MaxOrDefault(o => o.ValueReadingDate);

            if (value < maxValue)
            {
                // TODO: Besserer Exception-Typ.
                throw new Exception("...");
            }

            if (valueReadingDate < maxValueReadingDate)
            {
                throw new Exception("...");
            }

            var meterReceipt = new MeterReadingReceiptEntity(value, valueReadingDate);
            _meterReceipts.Add(meterReceipt);
            _addedItems.Add(meterReceipt);
        }

        /// <summary>
        /// Removes the meter reading receipt with the highest value.
        /// If multiple readings shares the highest value then the newest is deleted.
        /// </summary>
        public void RemoveNewestMeterReadingReceipt()
        {
            if (!_meterReceipts.Any()) throw new FormatException("Remove Receipt not possible because no receipt exists.");

            var newestMeterReceipt = _meterReceipts.MaxBy(o => o.Value);
            _meterReceipts.Remove(newestMeterReceipt!);
        }

        // Experimental

        private readonly List<IDomainEntity> _addedItems = new();
        public IReadOnlyCollection<IDomainEntity> AddedItems { get => _addedItems.AsReadOnly(); }
        private readonly List<IDomainEntity> _removedItems = new();
        public IReadOnlyCollection<IDomainEntity> RemovedItems { get => _removedItems.AsReadOnly(); }
    }
}
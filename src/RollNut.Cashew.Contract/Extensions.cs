using System.Text.RegularExpressions;

namespace RollNut.Cashew.Contract
{
    public static class Extensions
    {
        // public static decimal MinOrDefault<T>(this IEnumerable<T> source, Func<T, decimal> selector)
        // {
        //     return source.Any() ? source.Min(selector) : default(decimal);
        // }
        // public static decimal MaxOrDefault<T>(this IEnumerable<T> source, Func<T, decimal> selector)
        // {
        //     return source.Any() ? source.Max(selector) : default(decimal);
        // }
        public static TResult? MaxOrDefault<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, TResult> selector)
        {
            return source.Any() ? source.Max(selector) : default(TResult);
        }

        /// <summary>
        /// Split string by camel cased rules (e.g. MyClassName -> My Class Name).
        /// </summary>
        /// <param name="value">The string to split by camel case rules.</param>
        /// <param name="separator">The separator between the camel case splits.</param>
        public static string SplitCamelCase(this string value, string separator)
        {
            return Regex.Replace(value, @"(\B[A-Z]+?(?=[A-Z][^A-Z])|\B[A-Z]+?(?=[^A-Z]))", separator + "$1");
        }

    }
}
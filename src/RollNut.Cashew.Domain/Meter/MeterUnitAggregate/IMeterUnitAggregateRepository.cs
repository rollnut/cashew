namespace RollNut.Cashew.Domain.Meter.MeterUnitAggregate
{
    /// <summary>
    /// Api-definition to persistence medium for <see cref="MeterUnitAggregateRoot"/>.
    /// </summary>
    public interface IMeterUnitAggregateRepository : IRepository
    {
        /// <summary>
        /// Get the aggregate by id or null.
        /// </summary>
        /// <param name="id">The id of the aggregate root.</param>
        Task<MeterUnitAggregateRoot?> GetByKeyAsync(int id);

        /// <summary>
        /// Persist the aggregate to persistence medium.
        /// </summary>
        /// <param name="aggregateRoot">This aggregate should be persist.</param>
        Task UpdateAsync(MeterUnitAggregateRoot aggregateRoot);

        /// <summary>
        /// Delete the aggregate from persistence medium.
        /// </summary>
        /// <param name="aggregateRoot">This aggregate should be deleted.</param>
        Task DeleteAsync(MeterUnitAggregateRoot aggregateRoot);
    }
}
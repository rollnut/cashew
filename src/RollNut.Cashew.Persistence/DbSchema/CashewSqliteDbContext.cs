using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using RollNut.Cashew.Domain;

namespace RollNut.Cashew.Persistence.DbSchema
{
    /// <summary>
    /// DatabaseContext for Cashew with Sqllite as engine.
    /// </summary>
    /// <remarks>
    /// <see href="https://docs.microsoft.com/de-de/ef/core/get-started/overview/first-app?tabs=netcore-cli"/>
    /// </remarks>
    public class CashewSqliteDbContext : DbContext, IUnitOfWork
    {
        private const string _keydbpath = "sqlitepath";
        private static bool _isFirstDbAccess = false;

        public CashewSqliteDbContext(IConfiguration configuration)
        {
            if (configuration is null) throw new ArgumentNullException(nameof(configuration));
            
            var dbPath = configuration[_keydbpath];
            if (string.IsNullOrWhiteSpace(dbPath))
            {
                throw new FormatException($"Can not access sqlite database because the appsettings missing the path to the db file. Add the config key '{_keydbpath}' to appsettings.");
            }

            DbPath = dbPath;

            // Ensure a fresh db is created at program start.
            // This block is temporarly for the first time of the alpha version.
            if (!_isFirstDbAccess)
            {
                _isFirstDbAccess = true;

                var directory = Path.GetDirectoryName(dbPath);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                } 

                Database.EnsureDeleted();
                // The methode 'Database.EnsureDeleted' only deletes the db-file (*.db) but not the temp files. 
                // Temp files must be deleted too in order method 'Database.EnsureCreated' ist working.
                {
                    var tempFiles = new [] { "*.db-shm", "*.db-wal" }
                        .SelectMany(o => Directory.GetFiles(directory, o))
                        ;
                    if (tempFiles.Any())
                    {
                        tempFiles.ToList().ForEach(o => File.Delete(o));
                    }
                }
                Database.EnsureCreated();

                DbFactory.FillTestData(this);
            }   
        }

        public DbSet<DbMeta> DbMeta { get; set; }
        public DbSet<Member> Member { get; set; }
        public DbSet<MeterUnit> MeterUnit { get; set; }
        public DbSet<MeterReadingReceipt> MeterReadingReceipt { get; set; }
        
        public string DbPath { get; private set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($"Data Source={DbPath}");
        }

        #region IUnitOfWork

        public void Commit()
        {
            SaveChanges();
        }

        public async Task CommitAsync(CancellationToken cancellationToken)
        {
            await SaveChangesAsync(cancellationToken)
                .ConfigureAwait(false)
                ;
        }

        #endregion
    }
}
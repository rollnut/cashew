using System.ComponentModel.DataAnnotations;

namespace RollNut.Cashew.Contract.Meter
{
    public class MeterUnitDeleteCommand : IRequest<MeterUnitDeleteCommandResponse>
    {
        [Range(1, int.MaxValue)]
        public int Id { get; set; }
    }
    
    public class MeterUnitDeleteCommandResponse
    {
        public static MeterUnitDeleteCommandResponse Null = new();
    }    
}
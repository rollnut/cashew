namespace RollNut.Cashew.Domain
{
    public interface IDomainEntity
    {
        object Id { get; }
    }

    public interface IDomainEntity<T> : IDomainEntity
    {
        object IDomainEntity.Id { get => Id!; }
        new T Id { get; }
    }

    public interface IDomainValueObject
    {

    }

    public interface IAggregateRoot<T> : IDomainEntity<T>
    {        
    }

    public interface IUnitOfWork
    {
        void Commit();
        Task CommitAsync(CancellationToken cancellationToken);
    }

    public interface IRepository
    {
        IUnitOfWork UnitOfWork { get; }
    }

    // public interface IAggregateRepository<T_AggregateRoot, T_Key>
    //     where T_AggregateRoot : IAggregateRoot<T_Key>
    // {
    //     T_AggregateRoot GetByKey(T_Key key);
    // }
}
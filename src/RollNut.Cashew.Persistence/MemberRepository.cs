using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using RollNut.Cashew.Contract;
using RollNut.Cashew.Persistence.DbSchema;

namespace RollNut.Cashew.Domain
{
    public interface IRepository {}
    public interface IMemberRepository : IRepository
    {
        void Create(MemberDto viewModel);
        void Update(MemberDto viewModel);
        void Delete(int id);
    }

    public class MemberRepository : IMemberRepository
    {
        private readonly CashewSqliteDbContext _dbContext;
        private readonly IMapper _mapper;

        public MemberRepository(CashewSqliteDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public void Create(MemberDto viewModel)
        {
            ArgumentNullException.ThrowIfNull(viewModel);

            var dbItem = _mapper.Map<Member>(viewModel);
            _dbContext.Add(dbItem);
            _dbContext.SaveChanges();
        }

        public void Update(MemberDto viewModel)
        {
            ArgumentNullException.ThrowIfNull(viewModel);

            var dbItem = _dbContext.Member.First(o => o.Id == viewModel.Id);
            _mapper.Map(viewModel, dbItem);
            _dbContext.Update(dbItem);
            _dbContext.SaveChanges();            
        }

        public void Delete(int id)
        {
            var dbItem = _dbContext.Member.First(o => o.Id == id);
            if (dbItem != null)
            {
                _dbContext.Remove(dbItem);
            }
            _dbContext.SaveChanges();
        }
    }
}
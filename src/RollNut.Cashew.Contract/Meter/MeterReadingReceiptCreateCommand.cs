namespace RollNut.Cashew.Contract.Meter
{
    public class MeterReadingReceiptCreateCommand : IRequest<MeterReadingReceiptCreateCommandResponse>
    {
        [Min(1)]
        public int MeterUnitId { get; set; }
        public MeterReadingReceiptDto MeterReadingReceipt { get; set; }
    }
    
    public class MeterReadingReceiptCreateCommandResponse
    {
    }
}
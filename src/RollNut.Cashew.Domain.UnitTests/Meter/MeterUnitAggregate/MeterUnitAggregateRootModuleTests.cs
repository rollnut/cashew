using System;
using System.Collections.Generic;
using RollNut.Cashew.Contract;
using RollNut.Cashew.Contract.Meter;
using RollNut.Cashew.Domain.Meter.MeterUnitAggregate;

namespace RollNut.Cashew.Domain.UnitTests.Meter.MeterUnitAggregate
{
    public class MeterUnitAggregateRootModuleTests
    {
        /// <summary>
        /// This szenario tests the optimal way
        /// for editing the number of meter readings 
        /// by add and remove some of them.
        /// <list type="number">
        ///  <item>Create Meter Unit</item>
        ///  <item>Add five meter readings</item>
        ///  <item>Delete two meter readings</item>
        ///  <item>Add a meter reading again</item>
        /// </list>
        /// </summary>
        [Fact]
        public void Szenario1_CreateMeterUnitWithSomeMeterReadingsTest()
        {
            // Step 1: Create the Meter Unit.

            var aggregate = CreateAggregateRootMock(
                id: 1,
                meterUnitType: MeterUnitType.Water
            );

            Assert.Equal(1, aggregate.Id);
            Assert.Equal(MeterUnitType.Water, aggregate.MeterUnitType);
            Assert.Empty(aggregate.MeterReadingReceipts);

            // Step 2: Add five meter readings.

            aggregate.AddMeterReadingReceipt(1, new DateTime(2022, 02, 01));
            Assert.Single(aggregate.MeterReadingReceipts);

            aggregate.AddMeterReadingReceipt(3, new DateTime(2022, 02, 02));
            aggregate.AddMeterReadingReceipt(7, new DateTime(2022, 02, 03));
            aggregate.AddMeterReadingReceipt(13, new DateTime(2022, 02, 04));
            aggregate.AddMeterReadingReceipt(20, new DateTime(2022, 02, 05));

            Assert.Collection(aggregate.MeterReadingReceipts
                , mr => Assert.Equal(1, mr.Value)
                , mr => Assert.Equal(3, mr.Value)
                , mr => Assert.Equal(7, mr.Value)
                , mr => Assert.Equal(13, mr.Value)
                , mr => Assert.Equal(20, mr.Value)
            );

            // Step 3: Delete two meter readings.

            aggregate.RemoveNewestMeterReadingReceipt();
            aggregate.RemoveNewestMeterReadingReceipt();

            Assert.Collection(aggregate.MeterReadingReceipts
                , mr => Assert.Equal(1, mr.Value)
                , mr => Assert.Equal(3, mr.Value)
                , mr => Assert.Equal(7, mr.Value)
            );

              // Step 4: Add a meter reading again.

            aggregate.AddMeterReadingReceipt(12, new DateTime(2022, 02, 06));

            Assert.Collection(aggregate.MeterReadingReceipts
                , mr => Assert.Equal(1, mr.Value)
                , mr => Assert.Equal(3, mr.Value)
                , mr => Assert.Equal(7, mr.Value)
                , mr => Assert.Equal(12, mr.Value)
            );
        }

        // Methods - Mocking

        /// <summary>
        /// Creates a MeterUnitAggregateRoot with default values or not.
        /// </summary>
        private MeterUnitAggregateRoot CreateAggregateRootMock(
            int id = default, 
            MeterUnitType meterUnitType = default, 
            string name = "MockMeter XYZ-123", 
            MeasureUnit measureUnit = default, 
            decimal initialCount = default, 
            string remarks = "",
            IEnumerable<MeterReadingReceiptEntity>? meterReadingReceipts = default
        )
        {
            return new MeterUnitAggregateRoot(
                id: id, 
                meterUnitType: meterUnitType, 
                name: name, 
                measureUnit: measureUnit, 
                initialCount: initialCount, 
                remarks: remarks,
                meterReadingReceipts: meterReadingReceipts ?? new MeterReadingReceiptEntity[0]
            );
        }

        /// <summary>
        /// Creates a MeterReadingReceiptEntity with default values or not.
        /// </summary>
        private MeterReadingReceiptEntity CreateMeterReadingReceiptEntityMock(
            int id = default,
            decimal value = default, 
            DateTime valueReadingDate = default, 
            string remarks = ""
        )        
        {
            return new MeterReadingReceiptEntity(
                id: id,
                value: value, 
                valueReadingDate: valueReadingDate, 
                remarks: remarks
            );
        }
    }
}
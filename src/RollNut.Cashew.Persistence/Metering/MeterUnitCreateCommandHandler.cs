using System.ComponentModel.DataAnnotations;
using MediatR;
using RollNut.Cashew.Contract.Meter;
using RollNut.Cashew.Persistence.DbSchema;

namespace RollNut.Cashew.Persistence.Meter
{
    public class MeterUnitCreateCommandHandler : IRequestHandler<MeterUnitCreateCommand, MeterUnitCreateCommandResponse>
    {
        private readonly CashewSqliteDbContext _dbContext;

        public MeterUnitCreateCommandHandler(CashewSqliteDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }
    
        public async Task<MeterUnitCreateCommandResponse> Handle(MeterUnitCreateCommand request, CancellationToken cancellationToken)
        {
            ArgumentNullException.ThrowIfNull(request);
            Validator.ValidateObject(request, new ValidationContext(request), true);

            var table = new MeterUnit()
            {
                MeterUnitType = request.MeterUnitType,
                Name = request.Name,
                InitialValue = request.InitialValue,
            };
            _dbContext.MeterUnit.Add(table);
            _dbContext.SaveChanges();

            return new MeterUnitCreateCommandResponse();
        }
    }
}
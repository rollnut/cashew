namespace RollNut.Cashew.Contract.Meter
{
    public record MeterReadingReceiptDto
    {
        public int Id { get; init; }

        public int MeterUnitId { get; init; }

        /// <summary>
        /// Value for the receipt.
        /// Can be the new meter value or other things.
        /// Depends on <see cref="ReceiptType"/>.
        /// </summary>
        public decimal Value { get; init; }
        // public decimal MeterReading { get; set; }

        /// <summary>
        /// Logical date for <see cref="Value"/>.
        /// The value date is not a receipt date.
        /// </summary>
        public DateTime ValueDate { get; init; }

        public string? Remarks { get; init; }
    }
}
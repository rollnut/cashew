﻿using RollNut.Cashew.Web;

namespace RollNut.Cashew.SourceGenerator.Cli
{
    public class Program
    {
        public static string GetStamp = "// Warning: This file ist auto-generated. Do not touch it!";

        public static void Main()
        {
            string solutionDirectory = FindSolutionDirectory() ?? throw new ApplicationException("A solution file is required.");

            // Generate CashewLocalizer
            {
                //string path = Path.Combine("RollNut.Cashew.Web", $"{nameof(CashewLocalizer)}.generated.cs");
                string path = Path.Combine(solutionDirectory, "RollNut.Cashew.Web", $"{nameof(CashewLocalizer)}.generated.cs");

                //File.Create(path);
                using (var stream = new FileStream(path, FileMode.Create))
                using (var writer = new StreamWriter(stream))
                {
                    var localizer = new CashewLocalizerSourceGenerator(typeof(CashewLocalizer), writer);
                    localizer.Run();
                    stream.Flush();
                }
            }
        }

        // Methods - Helper

        /// <summary>
        /// Returns the absolute path to the directory where the next higher .sln file is contained.
        /// </summary>
        /// <param name="currentPath">Starting folder or null for current directory.</param>
        public static string? FindSolutionDirectory(string? currentPath = null)
        {
            currentPath ??= Directory.GetCurrentDirectory();
            var directory = new DirectoryInfo(currentPath);
            while (directory != null && !directory.GetFiles("*.sln").Any())
            {
                directory = directory.Parent;
            }
            return directory?.FullName;
        }
    }
}
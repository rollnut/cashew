using System.ComponentModel.DataAnnotations;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using RollNut.Cashew.Contract;
using RollNut.Cashew.Contract.Meter;
using RollNut.Cashew.Persistence.DbSchema;

namespace RollNut.Cashew.Domain.Queries
{    
    public class MeterReadingReceiptQueryHandler : IRequestHandler<MeterReadingReceiptQuery, MeterReadingReceiptQueryResponse>
    {
        private readonly CashewSqliteDbContext _dbContext;
        private readonly IMapper _mapper;

        public MeterReadingReceiptQueryHandler(CashewSqliteDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }
    
        public async Task<MeterReadingReceiptQueryResponse> Handle(MeterReadingReceiptQuery request, CancellationToken cancellationToken)
        {
            var meter = _dbContext.MeterUnit
                .Include(o => o.MeterReadingReceipts)
                .FirstOrDefault(o => o.Id == request.MeterUnitId)
                ;

            if (meter == null)
            {
                throw new ArgumentException($"Meter '{request.MeterUnitId}' not exists.");
            }

            var dtos = _mapper.Map<IEnumerable<MeterReadingReceiptDto>>(meter.MeterReadingReceipts);

            return new MeterReadingReceiptQueryResponse(dtos);
        }
    }
}
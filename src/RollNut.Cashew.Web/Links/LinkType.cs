namespace RollNut.Cashew.Web.Links
{
    public enum LinkType
    {
        Neutral,
        Positive,
        Negative,
    }
}
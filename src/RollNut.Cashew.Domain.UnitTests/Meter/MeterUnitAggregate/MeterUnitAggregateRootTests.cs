using System;
using System.Collections.Generic;
using RollNut.Cashew.Contract;
using RollNut.Cashew.Contract.Meter;
using RollNut.Cashew.Domain.Meter.MeterUnitAggregate;

namespace RollNut.Cashew.Domain.UnitTests.Meter.MeterUnitAggregate
{
    public class MeterUnitAggregateRootTests
    {
        [Fact]
        public void AddMeterReadingReceipt_NoExistingMeterReadingTest()
        {
            // Arrange

            var aggregate = CreateAggregateRootMock();

            // Act

            aggregate.AddMeterReadingReceipt(3, new DateTime(2022, 02, 05));

            // Assert

            Assert.Equal(1, aggregate.MeterReadingReceipts.Count);
        }

        [Fact]
        public void AddMeterReadingReceipt_OneExistingMeterReadingTest()
        {
            // Arrange

            var meterReading = CreateMeterReadingReceiptEntityMock(
                value: 5,
                valueReadingDate: new DateTime(2022, 02, 05)
            );
            var aggregate = CreateAggregateRootMock(
                meterReadingReceipts: new[] { meterReading }
            );

            // Act

            aggregate.AddMeterReadingReceipt(6, new DateTime(2022, 02, 06));

            // Assert

            Assert.Equal(2, aggregate.MeterReadingReceipts.Count);
        }

        [Fact]
        public void RemoveNewestMeterReadingReceipt_NoExistingReceipt_ExceptionTest()
        {
            // Arrange

            var aggregate = CreateAggregateRootMock();
            
            // Act / Arrange

            Assert.Throws<FormatException>(() => aggregate.RemoveNewestMeterReadingReceipt());
        }

        /// <summary>
        /// A fresh meter unit is always allowed to be deleted.
        /// </summary>
        [Fact]
        public void CanDelete_NoMeterReading_TrueTest()
        {
            // Arrange

            var aggregate = CreateAggregateRootMock();

            // Act

            var actual = aggregate.CanDelete();

            // Assert

            Assert.True(actual);
        }

        /// <summary>
        /// A fresh meter unit with a meter reading receipt is not allowed to be deleted.
        /// If the meter reading is removed then delete should work again.
        /// </summary>
        [Fact]
        public void CanDelete_OneMeterReading_FalseTest()
        {
            // Arrange

            var aggregate = CreateAggregateRootMock(meterReadingReceipts: new [] 
            {
                CreateMeterReadingReceiptEntityMock(),
            });

            // Act

            var actual = aggregate.CanDelete();

            // Assert

            Assert.False(actual);
        }

        // Methods - Mocking

        /// <summary>
        /// Creates a MeterUnitAggregateRoot with default values or not.
        /// </summary>
        private MeterUnitAggregateRoot CreateAggregateRootMock(
            int id = default, 
            MeterUnitType meterUnitType = default, 
            string name = "MockMeter XYZ-123", 
            MeasureUnit measureUnit = default, 
            decimal initialCount = default, 
            string remarks = "",
            IEnumerable<MeterReadingReceiptEntity>? meterReadingReceipts = default
        )
        {
            return new MeterUnitAggregateRoot(
                id: id, 
                meterUnitType: meterUnitType, 
                name: name, 
                measureUnit: measureUnit, 
                initialCount: initialCount, 
                remarks: remarks,
                meterReadingReceipts: meterReadingReceipts ?? new MeterReadingReceiptEntity[0]
            );
        }

        /// <summary>
        /// Creates a MeterReadingReceiptEntity with default values or not.
        /// </summary>
        private MeterReadingReceiptEntity CreateMeterReadingReceiptEntityMock(
            int id = default,
            decimal value = default, 
            DateTime valueReadingDate = default, 
            string remarks = ""
        )        
        {
            return new MeterReadingReceiptEntity(
                id: id,
                value: value, 
                valueReadingDate: valueReadingDate, 
                remarks: remarks
            );
        }
    }
}
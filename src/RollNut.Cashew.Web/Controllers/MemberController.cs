using System;
using System.Collections.Generic;
using System.Linq;
using RollNut.Cashew.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using RollNut.Cashew.Persistence.DbSchema;
using RollNut.Cashew.Domain;
using MediatR;
using RollNut.Cashew.Domain.Queries;
using RollNut.Cashew.Web.ViewModels.Base;
using RollNut.Cashew.Contract;
using AutoMapper;

namespace RollNut.Cashew.Web.Controllers
{
    [Controller]
    [Route("[controller]")]
    public class MemberController : Controller
    {
        private readonly CashewEssentials<MemberController> _essentials;
        private readonly IMemberRepository _repository;

        public MemberController(CashewEssentials<MemberController> essentials, IMemberRepository repository)
        {
            _essentials = essentials ?? throw new ArgumentNullException(nameof(essentials));
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        // List / Index

        [HttpGet("")]
        public async Task<ActionResult<IEnumerable<MemberViewModel>>> Get()
        {
            var request = new MemberQuery();
            var response = await _essentials.Mediator.Send(request);

            var viewModels = response.Result.Select(o => new MemberViewModel() { Id = o.Id, FirstName = o.FirstName, LastName = o.LastName});
            var viewModel = new MemberListViewModel(viewModels);

            return View("readlist", viewModel);
        }

        // Create

        [HttpGet("create")]
        public ActionResult<MemberViewModel> Create()
        {
            var viewModel = new MemberViewModel();
            return View(viewModel);
        }

        [HttpPost("create")]
        public ActionResult<MemberViewModel> CreatePostback(MemberViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(nameof(Create), viewModel);
            }

            var dto = _essentials.Mapper.Map<MemberDto>(viewModel);
            _repository.Create(dto);

            return RedirectToAction("");
        }

        // Read

        [HttpGet("{id}")]
        public async Task<ActionResult<MemberViewModel>> Get(int id)
        {
            var request = new MemberQuery()
            {
                MemberIds = new [] { id },
            };
            var response = await _essentials.Mediator.Send(request);
            var dto = response.Result.FirstOrDefault();

            if (dto == null)
            {
                return NotFound();
            }

            var viewModel = _essentials.Mapper.Map<MemberViewModel>(dto);

            return View("read", viewModel);
        }

        // Update

        [HttpGet("{id}/edit")]
        public async Task<ActionResult<MemberViewModel>> Update(int id)
        {
            var request = new MemberQuery()
            {
                MemberIds = new [] { id },
            };
            var response = await _essentials.Mediator.Send(request);
            var dto = response.Result.FirstOrDefault();

            if (dto == null)
            {
                return NotFound();
            }

            var viewModel = _essentials.Mapper.Map<MemberViewModel>(dto);

            return View("update", viewModel);
        }

        [HttpPost("{id}/edit")]
        public ActionResult<MemberViewModel> UpdatePostback(int id, MemberViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(nameof(Update), viewModel);
            }

            var dto = _essentials.Mapper.Map<MemberDto>(viewModel);
            _repository.Update(dto);

            return RedirectToAction("");
        }

        // Delete

        [HttpGet("{id}/delete")]
        public async Task<ActionResult<MemberViewModel>> Delete(int id)
        {
            var request = new MemberQuery()
            {
                MemberIds = new [] { id },
            };
            var response = await _essentials.Mediator.Send(request);
            var dto = response.Result.FirstOrDefault();

            if (dto == null)
            {
                return NotFound();
            }

            var viewModel = _essentials.Mapper.Map<MemberViewModel>(dto);
            return View("delete", viewModel);
        }

        [HttpPost("{id}/delete")]
        public ActionResult<MemberViewModel> DeletePostback(int id)
        {
            _repository.Delete(id);

            return RedirectToAction("");
        }
    }
}
using MediatR;

namespace RollNut.Cashew.Contract
{
    public record MemberQuery : IRequest<MemberQueryResponse>
    {
        /// <summary>
        /// If null then all available members gonna be load.
        /// </summary>
        public IEnumerable<int>? MemberIds { get; init; }
    }
    
    public record MemberQueryResponse
    {
        public MemberQueryResponse()
        {
            Result = Enumerable.Empty<MemberDto>();
        }

        public MemberQueryResponse(IEnumerable<MemberDto> members)
        {
            Result = members ?? throw new ArgumentNullException(nameof(members));
        }

        public IEnumerable<MemberDto> Result { get; init; }
    }
}
namespace RollNut.Cashew.Web.ViewModels.Base
{
    public interface IViewModel
    {
        string GetDisplayName();
    }

    public interface IEntityViewModel : IViewModel
    {
        object Id { get; }
    }

    public interface IEntityViewModel<T> : IEntityViewModel
    {
        object IEntityViewModel.Id { get => Id; }

        T Id { get; }
    }
}
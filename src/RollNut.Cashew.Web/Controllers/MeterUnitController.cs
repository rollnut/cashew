using System;
using System.Collections.Generic;
using System.Linq;
using RollNut.Cashew.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using RollNut.Cashew.Persistence.DbSchema;
using RollNut.Cashew.Domain;
using RollNut.Cashew.Web.ViewModels.Base;
using AutoMapper.QueryableExtensions;
using AutoMapper;
using MediatR;
using RollNut.Cashew.Domain.Queries;
using RollNut.Cashew.Contract.Meter;
using RollNut.Cashew.Web.ViewModels.Meter;

namespace RollNut.Cashew.Web.Controllers
{
    [Controller]
    [Route("[controller]")]
    public class MeterUnitController : Controller
    {
        private readonly CashewEssentials<MeterUnitController> _essentials;

        public MeterUnitController(CashewEssentials<MeterUnitController> essentials)
        {
            _essentials = essentials ?? throw new ArgumentNullException(nameof(essentials));
        }

        [HttpGet("")]
        public async Task<ActionResult<IEnumerable<MeterUnitViewModel>>> Get()
        {
            var request = new MeterUnitQuery()
            {
            };
            var response = await _essentials.Mediator.Send(request);
            var viewModels = _essentials.Mapper.Map<IEnumerable<MeterUnitViewModel>>(response.Results);
            var viewModel = new MeterUnitListViewModel(viewModels);

            return View("readlist", viewModel);
        }

        // Create

        [HttpGet("create")]
        public ActionResult<MemberViewModel> Create()
        {
            var viewModel = new MeterUnitViewModel();
            return View(viewModel);
        }

        [HttpPost("create")]
        public async Task<ActionResult<MeterUnitViewModel>> CreatePostback(MeterUnitViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(nameof(Create), viewModel);
            }

            var command = new MeterUnitCreateCommand(viewModel.MeterUnitType, viewModel.Name, viewModel.InitialValue);
            var response = await _essentials.Mediator.Send(command);

            return RedirectToAction("");
        }

        // Delete

        [HttpGet("{id}/delete")]
        public async Task<ActionResult<MeterUnitViewModel>> Delete(int id)
        {
            var command = new MeterUnitDeleteCommand()
            {
                Id = id,
            };

            var response = await _essentials.Mediator.Send(command);
            return RedirectToAction("");
        }
    }
}
using RollNut.Cashew.Web.Controllers;

namespace RollNut.Cashew.Web.Links
{
    /// <summary>
    /// Links which are possible for <see cref="MeterUnitController"/>.
    /// </summary>
    public class MeterUnitLinkLibrary : LinkLibraryBase<MeterUnitController>
    {        
        /// <summary>
        /// Show list of meter units.
        /// <para>
        /// The action <see cref="MeterUnitController.Get"/> is called.
        /// </para>
        /// </summary>
        public RollNutLink List()
        {
            return CreateControllerLink()
                .SetDisplayText("📂 Meter Unit-Area")
                ;
        }

        /// <summary>
        /// Show create form for a new meter unit.
        /// <para>
        /// The action <see cref="MeterUnitController.Create"/> is called.
        /// </para>
        /// </summary>
        public RollNutLink Create()
        {
            return CreateControllerLink(CREATE)
                .SetDisplayText("🟢 New Meter Unit")
                .SetLinkType(LinkType.Positive)
                ;
        }

        /// <summary>
        /// Show edit form for one meter unit.
        /// <para>
        /// The action <see cref="MemberController.Update(int)"/> is called.
        /// </para>
        /// </summary>
        public RollNutLink Update(int id)
        {
            return CreateControllerLink(id, UPDATE)
                .SetDisplayText("✏️ Meter Unit")
                ;
        }

        /// <summary>
        /// Show delete confirmation for one meter unit.
        /// <para>
        /// The action <see cref="MeterUnitController.Delete(int)"/> is called.
        /// </para>
        /// </summary>
        public RollNutLink Delete(int id)
        {
            return CreateControllerLink(id, DELETE)
                .SetDisplayText("❌ Meter Unit")
                .SetLinkType(LinkType.Negative)
                ;
        }
    }
}
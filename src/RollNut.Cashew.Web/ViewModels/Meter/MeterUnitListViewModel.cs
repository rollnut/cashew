using RollNut.Cashew.Web.ViewModels.Base;

namespace RollNut.Cashew.Web.ViewModels.Meter
{
    public class MeterUnitListViewModel : ListViewModelBase<MeterUnitViewModel>
    {
        public MeterUnitListViewModel(IEnumerable<MeterUnitViewModel> meters)
        {
            ArgumentNullException.ThrowIfNull(meters);
            AddItems(meters);

            var links = new LinkLibrary();
            ListActions.Add(links.MeterUnit.Create());
        }

        public override IEnumerable<string> GetColumnHeaders()
        {
            return new[]
            {
                "d",
                "",
                "Type",
                "Name",
                "Last 30 day consumption",
                "Complete consumption",
                "CurrentValue",
                "InitialValue",
            };
        }

        public override IEnumerable<object> GetColumnValues(MeterUnitViewModel viewModel)
        {
            var links = new LinkLibrary();

            return new object[]
            {
                links.MeterUnit.Delete(viewModel.Id).GetHtmlLink(),
                links.MeterReadingReceipt.List(viewModel.Id).GetHtmlLink(),
                viewModel.MeterUnitType,
                viewModel.GetDisplayName(),
                "na",
                "na",
                "na",// viewModel.CurrentValue,
                viewModel.InitialValue,
            };
        }
    }
}
﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Localization;
using Microsoft.Extensions.Localization;
using RollNut.Cashew.Web.Base;

namespace RollNut.Cashew.Web
{
    /// <summary>
    /// Access localized resources.
    /// </summary>
    public partial class CashewLocalizer
    {
        [LocalizeType(LocalizeType.String)]
        public const string WELCOME = "Welcome";

        [LocalizeType(LocalizeType.HtmlString)]
        public const string WELCOME_HTML = "WelcomeHtml";

        /// <inheritdoc cref="CashewLocalizer"/>
        public CashewLocalizer(IStringLocalizer<CashewLocalizer> stringLocalizer, IHtmlLocalizer<CashewLocalizer> htmlLocalizer)
        {
            StringLocalizer = stringLocalizer ?? throw new ArgumentNullException(nameof(stringLocalizer));
            HtmlLocalizer = htmlLocalizer ?? throw new ArgumentNullException(nameof(htmlLocalizer));
        }

        /// <summary>
        /// Accessor to get global localized strings.
        /// </summary>
        public IStringLocalizer<CashewLocalizer> StringLocalizer { get; }

        /// <summary>
        /// Accessor to get global localized html strings.
        /// </summary>
        public IHtmlLocalizer<CashewLocalizer> HtmlLocalizer { get; }
    }
}

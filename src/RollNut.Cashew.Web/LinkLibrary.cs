using RollNut.Cashew.Contract;
using RollNut.Cashew.Web.Links;

namespace RollNut.Cashew.Web
{
    public class LinkLibrary : LinkLibraryBase
    {
        /// <inheritdoc cref="MemberLinkLibrary"/>
        public MemberLinkLibrary Member { get; } = new();

        /// <inheritdoc cref="MeterUnitLinkLibrary"/>
        public MeterUnitLinkLibrary MeterUnit { get; } = new();

        /// <inheritdoc cref="MeterReadingReceiptLinkLibrary"/>
        public MeterReadingReceiptLinkLibrary MeterReadingReceipt { get; } = new();

        
        // // Member


        // public RollNutLink MemberList()
        // {
        //     var controllerName = GetControllerName(nameof(MemberController));
        //     var link = Concatenate(controllerName);
        //     return new RollNutLink(link);
        // }

        // public RollNutLink MemberCreate()
        // {
        //     var controller = GetControllerName(nameof(MemberController));
        //     var link = Concatenate(controller, Create);
        //     return new RollNutLink(link)
        //         .AddCssClass("positive")
        //         ;
        // }

        // public RollNutLink MemberRead(int id)
        // {
        //     var controller = GetControllerName(nameof(MemberController));
        //     var link = Concatenate(controller, id);
        //     return new RollNutLink(link);
        // }

        // public RollNutLink MemberUpdate(int id)
        // {
        //     var controller = GetControllerName(nameof(MemberController));
        //     var link = Concatenate(controller, Update, id);
        //     return new RollNutLink(link);
        // }

        // public RollNutLink MemberDelete(int id)
        // {
        //     var controller = GetControllerName(nameof(MemberController));
        //     var link = Concatenate(controller, Delete, id);
        //     return new RollNutLink(link);
        // }

        // MeterUnit

        // public RollNutLink MeterUnitList()
        // {
        //     var meterUnitController = GetControllerName(nameof(MeterUnitController));
        //     var link = Concatenate(meterUnitController);
        //     return new RollNutLink(link, GetDisplayText(meterUnitController));
        // }

        // public string MeterUnitEdit(int meterUnitId)
        // {
        //     var meterUnitController = GetControllerName(nameof(MeterUnitController));
        //     return Concatenate(meterUnitController, meterUnitId);
        // }

        // public string MeterReadingReceiptDelete(int meterId, int id)
        // {
        //     var meterController = GetControllerName(nameof(MeterUnitController));
        //     var controller = GetControllerName(nameof(MeterReadingReceiptController));
        //     return Concatenate(meterController, meterId, controller, Delete, id);
        // }

        // MeterReadingReceiptReceipt

        // public string MeterReadingReceiptList()
        // {
        //     return GetControllerName(nameof(MeterReadingReceiptController));
        // }

        // public string MeterReadingReceiptCreate()
        // {
        //     var controller = GetControllerName(nameof(MeterReadingReceiptController));
        //     return Concatenate(controller, Create);
        // }

        // public string MeterReadingReceiptRead(int id)
        // {
        //     var controller = GetControllerName(nameof(MeterReadingReceiptController));
        //     return Concatenate(controller, Read, id);
        // }

        // public string MeterReadingReceiptDelete(int id)
        // {
        //     var controller = GetControllerName(nameof(MeterReadingReceiptController));
        //     return Concatenate(controller, Delete, id);
        // }

        // Methods - Helper

        private string Concatenate(params object[] urlSegemnts)
        {
            if (urlSegemnts.Any(o => o is null)) throw new ArgumentException("An item in given array is null. Null values are not allowed.", nameof(urlSegemnts));
            return string.Join("/", urlSegemnts.Select(o => o.ToString().ToLower()));
        }

        private string GetDisplayText(string key)
        {
            if (string.IsNullOrWhiteSpace(key)) throw new ArgumentException($"'{nameof(key)}' cannot be null or whitespace.", nameof(key));

            // TODO: Get link name from translation file.
            return key.SplitCamelCase(" ");
        }
    }
}
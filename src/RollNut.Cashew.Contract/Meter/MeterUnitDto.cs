namespace RollNut.Cashew.Contract.Meter
{
    public record MeterUnitDto
    {
        public int Id { get; init; }

        [Min(1)]
        /// <inheritdoc cref="RollNut.Cashew.Domain.DbSchema.MeterType" />
        public MeterUnitType MeterUnitType { get; init; }

        [Required]
        public string Name { get; init; } = string.Empty;

        // [Min(0L)]
        public long InitialValue { get; init; }
    }
}
using RollNut.Cashew.Web.Controllers.Meter;

namespace RollNut.Cashew.Web.Links
{
    /// <summary>
    /// Links which are possible for <see cref="MeterReadingReceiptController"/>.
    /// </summary>
    public class MeterReadingReceiptLinkLibrary : LinkLibraryBase<MeterReadingReceiptController>
    {
        protected override string GetBaseLink()
        {
            return Concatenate("meterunit", "{meterunitid}", base.GetBaseLink());;
        }

        protected RollNutLink CreateControllerLink(int meterUnitId, params object[] pathSegments)
        {
            // This is a ugly style for the moment.
            var link = base.CreateControllerLink(pathSegments);
            var newLink = link.Link.Replace("{meterunitid}", meterUnitId.ToString());
            return new RollNutLink(newLink);
        }

        /// <summary>
        /// Show list of members.
        /// <para>
        /// The action <see cref="MeterReadingReceiptController.Get(int)"/> is called.
        /// </para>
        /// </summary>
        public RollNutLink List(int meterUnitId)
        {
            return CreateControllerLink(meterUnitId)
                .SetDisplayText("📂 Meter Reading Receipt-Area")
                ;
        }

        /// <summary>
        /// Show create form for a new member reading receipt.
        /// <para>
        /// The action <see cref="MeterReadingReceiptController.Create(int)"/> is called.
        /// </para>
        /// </summary>
        /// <param name="meterUnitId">Id of the meter unit not for the meter reading.</param>
        public RollNutLink Create(int meterUnitId)
        {
            return CreateControllerLink(meterUnitId, nameof(MeterReadingReceiptController.Create))
                .SetDisplayText("🟢 New Meter Reading Receipt")
                .SetLinkType(LinkType.Positive)
                ;
        }

        /// <summary>
        /// Show delete confirmation for the newest meter reading receipt.
        /// <para>
        /// The action 'Coming Soon' is called.
        /// </para>
        /// </summary>      
        /// <param name="meterUnitId">Id of the meter unit not for the meter reading.</param>
        public RollNutLink DeleteNewest(int meterUnitId)
        {
            return CreateControllerLink(meterUnitId, "deletenewest")
                .SetDisplayText("❌ Delete Newest Meter Reading Receipt")
                .SetLinkType(LinkType.Negative)
                ;
        }        
    }
}
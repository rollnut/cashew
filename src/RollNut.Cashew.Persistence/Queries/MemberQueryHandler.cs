using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using RollNut.Cashew.Contract;
using RollNut.Cashew.Persistence.DbSchema;

namespace RollNut.Cashew.Domain.Queries
{    
    public class MemberQueryHandler : IRequestHandler<MemberQuery, MemberQueryResponse>
    {
        private readonly CashewSqliteDbContext _dbContext;
        private readonly IMapper _mapper;

        public MemberQueryHandler(CashewSqliteDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }
    
        public async Task<MemberQueryResponse> Handle(MemberQuery request, CancellationToken cancellationToken)
        {
            var query = _dbContext.Member.AsQueryable();
            
            if (request.MemberIds?.Any() == true)
            {
                query = query.Where(o => request.MemberIds.Contains(o.Id));
            }

            var result = query
                .ProjectTo<MemberDto>(_mapper.ConfigurationProvider)
                .ToList()
                ;
            return new MemberQueryResponse(result);
        }
    }
}
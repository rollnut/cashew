using RollNut.Cashew.Web.Controllers;

namespace RollNut.Cashew.Web.Links
{
    /// <summary>
    /// Links which are possible for <see cref="MemberController"/>.
    /// </summary>
    public class MemberLinkLibrary : LinkLibraryBase<MemberController>
    {
        /// <summary>
        /// Show list of members.
        /// <para>
        /// The action <see cref="MemberController.Get"/> is called.
        /// </para>
        /// </summary>
        public RollNutLink List()
        {
            return CreateControllerLink()
                .SetDisplayText("📂 Member-Area")
                ;
        }

        /// <summary>
        /// Show create form for a new member.
        /// <para>
        /// The action <see cref="MemberController.Create"/> is called.
        /// </para>
        /// </summary>
        public RollNutLink Create()
        {
            return CreateControllerLink(CREATE)
                .SetDisplayText("🟢 New Member")
                .SetLinkType(LinkType.Positive)
                ;
        }

        /// <summary>
        /// Show details for one member.
        /// <para>
        /// The action <see cref="MemberController.Get(int)"/> is called.
        /// </para>
        /// </summary>
        public RollNutLink Read(int id)
        {
            return CreateControllerLink(id)
                .SetDisplayText("👁️ Member Details")
                ;
        }

        /// <summary>
        /// Show edit form for one member.
        /// <para>
        /// The action <see cref="MemberController.Update(int)"/> is called.
        /// </para>
        /// </summary>
        public RollNutLink Update(int id)
        {
            return CreateControllerLink(id, UPDATE)
                .SetDisplayText("✏️ Edit Member")
                ;
        }

        /// <summary>
        /// Show delete confirmation for one member.
        /// <para>
        /// The action <see cref="MemberController.Delete(int)"/> is called.
        /// </para>
        /// </summary>
        public RollNutLink Delete(int id)
        {
            return CreateControllerLink(id, DELETE)
                .SetDisplayText("❌ Delete Member")
                .SetLinkType(LinkType.Negative)
                ;
        }
    }
}
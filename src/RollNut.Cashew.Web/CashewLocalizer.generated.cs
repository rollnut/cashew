// Warning: This file ist auto-generated. Do not touch it!
using System;
using Microsoft.AspNetCore.Html;

namespace RollNut.Cashew.Web
{
    public partial class CashewLocalizer
    {
        /// <summary>Returns localized <see cref='String'/> by constant <see cref='WELCOME'/> with key "Welcome".</summary>
        public String GetWelcome()
        {
            return StringLocalizer[WELCOME];
        }
        
        /// <summary>Returns localized <see cref='HtmlString'/> by constant <see cref='WELCOME_HTML'/> with key "WelcomeHtml".</summary>
        public HtmlString GetWelcomeHtml()
        {
            return new HtmlString(HtmlLocalizer[WELCOME_HTML].Value);
        }
        
    }
}

using AutoMapper;
using MediatR;

namespace RollNut.Cashew.Web
{
    /// <summary>
    /// In order to reduce constructor parameter length for dependency injection
    /// this class was created.
    /// Some components which each controller needs are contained here.
    /// </summary>
    /// <typeparam name="T">The calling class (will be the category for the logger).</typeparam>
    public class CashewEssentials<T>
    {
        public CashewEssentials(ILogger<T> logger, IMediator mediator, IMapper mapper)
        {
            Logger = logger ?? throw new ArgumentNullException(nameof(logger));
            Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            Mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public ILogger<T> Logger { get; }
        public IMediator Mediator { get; }
        public IMapper Mapper { get; set; }
    }
}
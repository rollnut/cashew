using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using RollNut.Cashew.Contract;
using RollNut.Cashew.Web.Controllers;
using RollNut.Cashew.Domain.Queries;

namespace RollNut.Cashew.Web.Api.v1
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class MemberController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public MemberController(IMediator mediator, IMapper mapper)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        /// <summary>
        /// Get all available member.
        /// </summary>
        /// <returns></returns>
        [HttpGet("")]
        public async Task<IEnumerable<MemberDto>> GetMember()
        {
            var request = new MemberQuery();
            var response = await _mediator.Send(request);
            // var dtos = _mapper.Map<IEnumerable<MemberDto>>(response.Result);
            var dtos = response.Result.Select(o => new MemberDto()
            {
                Id = o.Id,
                Name = o.LastName + " " + o.FirstName,
            });
            return dtos;
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<ActionResult<IEnumerable<MemberDto>>> GetMember(int id)
        {
            var request = new MemberQuery()
            {
                MemberIds = new [] { id },
            };
            var response = await _mediator.Send(request);

            if (!response.Result.Any())
            {
                return NotFound();
            }

            // var dtos = _mapper.Map<IEnumerable<MemberDto>>(response.Result);
            var source = response.Result.Single();
            var dto = new MemberDto()
            {
                Id = source.Id,
                Name = source.LastName + " " + source.FirstName,
            };
            return Ok(dto);
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using RollNut.Cashew.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using RollNut.Cashew.Persistence.DbSchema;
using RollNut.Cashew.Domain;
using RollNut.Cashew.Web.ViewModels.Base;
using AutoMapper.QueryableExtensions;
using AutoMapper;
using MediatR;
using RollNut.Cashew.Domain.Queries;
using RollNut.Cashew.Contract;
using RollNut.Cashew.Contract.Meter;
using RollNut.Cashew.Web.ViewModels.Meter;

namespace RollNut.Cashew.Web.Controllers.Meter
{
    [Controller]
    [Route("meterunit/{meterunitid}/[controller]")]
    public class MeterReadingReceiptController : Controller
    {
        private readonly CashewEssentials<MeterReadingReceipt> _essentials;

        public MeterReadingReceiptController(CashewEssentials<MeterReadingReceipt> essentials)
        {
            _essentials = essentials ?? throw new ArgumentNullException(nameof(essentials));
        }

        [HttpGet()]
        public async Task<ActionResult<IEnumerable<MeterReadingReceiptViewModel>>> Get(int meterUnitId)
        {
            var request = new MeterReadingReceiptQuery()
            {
                MeterUnitId = meterUnitId,
            };
            var response = await _essentials.Mediator.Send(request);
            var viewModels = _essentials.Mapper.Map<IEnumerable<MeterReadingReceiptViewModel>>(response.Results);

            var listViewModel = new MeterReadingReceiptListViewModel(meterUnitId, MeterUnitType.Undefined, "", viewModels);

            return View("readlist", listViewModel);
        }        

        // Create

        [HttpGet("create")]
        public ActionResult<MeterReadingReceiptViewModel> Create(int meterUnitId)
        {
            var viewModel = new MeterReadingReceiptViewModel();
            return View(viewModel);
        }

        [HttpPost("create")]
        public async Task<ActionResult<MeterReadingReceiptViewModel>> CreatePostbackAsync(int meterUnitId, MeterReadingReceiptViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(nameof(Create), viewModel);
            }

            var dto = new MeterReadingReceiptDto()
            {
                MeterUnitId = meterUnitId,
                Value = viewModel.Value,
                ValueDate = viewModel.ValueDate,
            };
            var command = new MeterReadingReceiptCreateCommand()
            {
                MeterUnitId = meterUnitId,
                MeterReadingReceipt = dto,
            };
            var response = await _essentials.Mediator.Send(command);

            var links = new LinkLibrary();
            return Redirect(links.MeterReadingReceipt.List(meterUnitId).Link);
        }

        // Delete

        // [HttpGet("delete/{id}")]
        // public async Task<ActionResult<MeterReceiptViewModel>> Delete(int id)
        // {
        //     // var request = new MemberQuery()
        //     // {
        //     //     MemberIds = new [] { id },
        //     // };
        //     // var response = await _mediator.Send(request);
        //     // var viewModel = response.Result.FirstOrDefault();

        //     // if (viewModel == null)
        //     // {
        //     //     return NotFound();
        //     // }


        //     return View("delete", viewModel);
        // }

        // [HttpPost("delete/{id}")]
        // public ActionResult<MeterReceiptViewModel> DeletePostback(int id)
        // {
        //     // _repository.Delete(id);

        //     return RedirectToAction("");
        // }
    }
}
using RollNut.Cashew.Contract.Meter;
using RollNut.Cashew.Domain.Meter.MeterUnitAggregate;

namespace RollNut.Cashew.Domain.Meter
{
    public class MeterUnitDeleteCommandHandler : IRequestHandler<MeterUnitDeleteCommand, MeterUnitDeleteCommandResponse>
    {
        private readonly IMeterUnitAggregateRepository _repository;

        public MeterUnitDeleteCommandHandler(IMeterUnitAggregateRepository repository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }
    
        public async Task<MeterUnitDeleteCommandResponse> Handle(MeterUnitDeleteCommand request, CancellationToken cancellationToken)
        {
            var aggregate = await _repository.GetByKeyAsync(request.Id);
            
            if (aggregate != null && aggregate.CanDelete())
            {
                await _repository.DeleteAsync(aggregate);
                await _repository.UnitOfWork.CommitAsync(cancellationToken);
            }
            else
            {
                throw new ApplicationException("Cannot delete.");
            }

            return MeterUnitDeleteCommandResponse.Null;
        }
    }
}
using MediatR;
using RollNut.Cashew.Contract;

namespace RollNut.Cashew.Contract.Meter
{
    public record MeterUnitQuery : IRequest<MeterUnitQueryResponse>
    {
    }

    public record MeterUnitQueryResponse
    {
        public MeterUnitQueryResponse(IEnumerable<MeterUnitDto> results)
        {
            Results = results ?? throw new ArgumentNullException(nameof(results));
        }
        
        public IEnumerable<MeterUnitDto> Results { get; init; } = default!;
    }
}